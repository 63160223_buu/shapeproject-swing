/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeprojectcircle;

/**
 *
 * @author focus
 */
public class Triangle extends Shape{
    private double high;
    private double base;

    public Triangle(double high, double base) {
        super("Triangle");
        this.high = high;
        this.base = base;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    @Override
    public double calArea() {
        return 0.5 * high * base ;
    }

    @Override
    public double calPerimeter() {
        return base + high +high;
    }
    
}
