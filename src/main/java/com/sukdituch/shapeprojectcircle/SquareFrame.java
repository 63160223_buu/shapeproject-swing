/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeprojectcircle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author focus
 */
public class SquareFrame extends JFrame {// สืบทอดจาก JFrame

    JLabel lblSide; // กำหนดให้เป็น Attribute เพื่อที่จะสามารถเรียกใช้ที่ไหนก็ได้
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResult;

    public SquareFrame() {
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblSide = new JLabel("Side : ", JLabel.TRAILING); // Trailing คือ ชิดไปทางขวา
        lblSide.setSize(50, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);

        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        this.add(txtSide);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        btnCalculate.setForeground(Color.white);
        btnCalculate.setBackground(Color.pink);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResult = new JLabel("Square = ??? area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        this.add(lblResult);

        //โปรแกรมที่เราเขียนอยู่เรียกว่า Eventt Driven
        //เมื่อปุ่มถูกกดจะทำให้มาเรียก Object นี้ทำงาน
        btnCalculate.addActionListener(new ActionListener() { // ปกติแล้ว ActionListener มัน new Object ไม่ได้ เลยต้องใช้ {} (Anonymous Class มาช่วย) 
            @Override
            public void actionPerformed(ActionEvent e) {
                try {// ลองทำโปร Method นี้ซำ้อีกรอบ

                    String strSide = txtSide.getText();

                    double side = Double.parseDouble(strSide); // เมื่อใส่ตัวหนังเสือไปแล้ว เกิด error คือ NumberFormatException

                    Square square = new Square(side);
                    //4. อัพเดท lblResult โดยนำข้อมูลจาก Square ไปแสดงให้ครบ
                    lblResult.setText("Square side = " + String.format("%.2f", square.getSide())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " Perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {// ดักจับ Exception 
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error : Please input number" // SquareFrame.this คือ เรียกใช้ SquareFrame ถ้าใช้ this เฉยๆ จะเรียกใช้ Action Listener
                            ,
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
