/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeprojectcircle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author focus
 */
public class TriangleFrame extends JFrame {

    JLabel lblhigh; // กำหนดให้เป็น Attribute เพื่อที่จะสามารถเรียกใช้ที่ไหนก็ได้
    JLabel lblbase;
    JTextField txthigh;
    JTextField txtbase;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.init();
    }

    public void createLabelhighAndbase() {
        lblhigh = new JLabel("High : ", JLabel.TRAILING); // Trailing คือ ชิดไปทางขวา
        lblhigh.setSize(50, 20);
        lblhigh.setLocation(5, 5);
        lblhigh.setBackground(Color.WHITE);
        lblhigh.setOpaque(true);
        this.add(lblhigh);

        lblbase = new JLabel("Base : ", JLabel.TRAILING); // Trailing คือ ชิดไปทางขวา
        lblbase.setSize(50, 20);
        lblbase.setLocation(120, 5);
        lblbase.setBackground(Color.WHITE);
        lblbase.setOpaque(true);
        this.add(lblbase);
    }
    
    public void createTextFieldhighAndbase(){
        txthigh = new JTextField();
        txthigh.setSize(50, 20);
        txthigh.setLocation(60, 5);
        this.add(txthigh);

        txtbase = new JTextField();
        txtbase.setSize(50, 20);
        txtbase.setLocation(175, 5);
        this.add(txtbase);
    }
    
    public void createButtonCalculate(){
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(100, 35);
        btnCalculate.setForeground(Color.white);
        btnCalculate.setBackground(Color.pink);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);
    }

    public void createLabelResult(){
        
        lblResult = new JLabel("Triangle = ??? area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 65);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        this.add(lblResult);
    }
    
    public void init() {

        this.setSize(350, 350);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        this.createLabelhighAndbase();
        
        this.createTextFieldhighAndbase();
        
        this.createButtonCalculate();
        
        this.createLabelResult();

        //โปรแกรมที่เราเขียนอยู่เรียกว่า Eventt Driven
        //เมื่อปุ่มถูกกดจะทำให้มาเรียก Object นี้ทำงาน
        btnCalculate.addActionListener(new ActionListener() { // ปกติแล้ว ActionListener มัน new Object ไม่ได้ เลยต้องใช้ {} (Anonymous Class มาช่วย) 
            @Override
            public void actionPerformed(ActionEvent e) {
                try {// ลองทำโปร Method นี้ซำ้อีกรอบ

                    String strhigh = txthigh.getText();
                    String strbase = txtbase.getText();

                    double high = Double.parseDouble(strhigh); // เมื่อใส่ตัวหนังเสือไปแล้ว เกิด error คือ NumberFormatException
                    double base = Double.parseDouble(strbase);

                    Triangle triangle = new Triangle(high, base);
                    //4. อัพเดท lblResult โดยนำข้อมูลจาก Square ไปแสดงให้ครบ
                    lblResult.setText("High = " + String.format("%.2f", triangle.getHigh())
                            + " Base = " + String.format("%.2f", triangle.getBase())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {// ดักจับ Exception 
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input number" // SquareFrame.this คือ เรียกใช้ SquareFrame ถ้าใช้ this เฉยๆ จะเรียกใช้ Action Listener
                            ,
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txthigh.setText("");
                    txtbase.setText("");
                    txthigh.requestFocus();

                }
            }
        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
