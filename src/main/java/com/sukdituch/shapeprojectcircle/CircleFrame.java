/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeprojectcircle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author focus
 */
public class CircleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Circle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);// ตั้งเป็น null กล่าวคือใครก็ตามที่เข้ามาใช้ ต้องเป็นคนจัดรูปแบบเอง

        JLabel lblRadius = new JLabel("radius : ", JLabel.TRAILING); // Trailing คือ ชิดไปทางขวา
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);

        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        frame.add(txtRadius);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        btnCalculate.setForeground(Color.white);
        btnCalculate.setBackground(Color.pink);
        btnCalculate.setOpaque(true);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Circle Radius= ??? area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        //โปรแกรมที่เราเขียนอยู่เรียกว่า Eventt Driven
        //เมื่อปุ่มถูกกดจะทำให้มาเรียก Object นี้ทำงาน
        btnCalculate.addActionListener(new ActionListener() { // ปกติแล้ว ActionListener มัน new Object ไม่ได้ เลยต้องใช้ {} (Anonymous Class มาช่วย) 
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                
                //1. ดึงข้อมูล text จาก  txtRadius ไปใส่ใน strRadius
                String strRadius = txtRadius.getText();
                //2. แปลงstrRadius ให้เป็น radius: double praseDouble
                double radius = Double.parseDouble(strRadius); // เมื่อใส่ตัวหนังเสือไปแล้ว เกิด error คือ NumberFormatException
                //3. เมื่อแปลงเสร็จแล้วนำมาสร้าง instance object Circle(radius) กำหนดตัวแปรเป็น Circle
                Circle circle = new Circle(radius);
                //4. อัพเดท lblResult โดยนำข้อมูลจาก Circle ไปแสดงให้ครบ
                lblResult.setText("Circle r = " + String.format("%.2f", circle.getRadius())  
                        + " area = " + String.format("%.2f", circle.calArea())
                        + " Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                    } catch(Exception ex){// ดักจับ Exception 
                        JOptionPane.showMessageDialog(frame, "Error : Please input number"
                                , "Error", JOptionPane.ERROR_MESSAGE);
                        txtRadius.setText("");
                        txtRadius.requestFocus();
                    }
                }
        });

        frame.setVisible(true);
    }
}
