/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeprojectcircle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author focus
 */
public class RectangleFrame extends JFrame{
    JLabel lblwide; // กำหนดให้เป็น Attribute เพื่อที่จะสามารถเรียกใช้ที่ไหนก็ได้
    JLabel lbllength;
    JTextField txtwide;
    JTextField txtlength;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(350, 350);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblwide = new JLabel("wide : ", JLabel.TRAILING); // Trailing คือ ชิดไปทางขวา
        lblwide.setSize(50, 20);
        lblwide.setLocation(5, 5);
        lblwide.setBackground(Color.WHITE);
        lblwide.setOpaque(true);
        this.add(lblwide);

        txtwide = new JTextField();
        txtwide.setSize(50, 20);
        txtwide.setLocation(60, 5);
        this.add(txtwide);
        
        lbllength = new JLabel("length : ", JLabel.TRAILING); // Trailing คือ ชิดไปทางขวา
        lbllength.setSize(50, 20);
        lbllength.setLocation(120, 5);
        lbllength.setBackground(Color.WHITE);
        lbllength.setOpaque(true);
        this.add(lbllength);
        
        txtlength = new JTextField();
        txtlength.setSize(50, 20);
        txtlength.setLocation(175, 5);
        this.add(txtlength);


        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(100, 35);
        btnCalculate.setForeground(Color.white);
        btnCalculate.setBackground(Color.pink);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle = ??? area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 65);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        //โปรแกรมที่เราเขียนอยู่เรียกว่า Eventt Driven
        //เมื่อปุ่มถูกกดจะทำให้มาเรียก Object นี้ทำงาน
        btnCalculate.addActionListener(new ActionListener() { // ปกติแล้ว ActionListener มัน new Object ไม่ได้ เลยต้องใช้ {} (Anonymous Class มาช่วย) 
            @Override
            public void actionPerformed(ActionEvent e) {
                try {// ลองทำโปร Method นี้ซำ้อีกรอบ

                    String strwide = txtwide.getText();
                    String strlength = txtlength.getText();

                    double wide = Double.parseDouble(strwide); // เมื่อใส่ตัวหนังเสือไปแล้ว เกิด error คือ NumberFormatException
                    double length = Double.parseDouble(strlength); 

                    Rectangle rectangle = new Rectangle(wide, length);
                    //4. อัพเดท lblResult โดยนำข้อมูลจาก Square ไปแสดงให้ครบ
                    lblResult.setText("wide = " + String.format("%.2f", rectangle.getWide())
                            + " length = " + String.format("%.2f", rectangle.getLength())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {// ดักจับ Exception 
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error : Please input number" // SquareFrame.this คือ เรียกใช้ SquareFrame ถ้าใช้ this เฉยๆ จะเรียกใช้ Action Listener
                            ,"Error", JOptionPane.ERROR_MESSAGE);
                    txtwide.setText("");
                    txtlength.setText("");
                    txtwide.requestFocus();
              
                
                }}
        });
    }
                
                
                
        public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
