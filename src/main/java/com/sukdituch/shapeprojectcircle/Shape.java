/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.shapeprojectcircle;

/**
 *
 * @author focus
 */
public abstract class Shape {// abstract เป็นตัวต้นแบบที่ให้คลาสอื่นๆนำไปใช้ต่อ
    
    private String shapeName;

    public Shape(String shapeName) {
        this.shapeName = shapeName;
    }

    public String getShapeName() {
        return shapeName;
    }
    
    public abstract double calArea();
    public abstract double calPerimeter();
}
